package cl.ubb.test1;

public class Conversor {
	public static double Conversor(String tipo,double grado) {
		double gradoTransformado;
		if(tipo=="Celcius") {
			gradoTransformado=(grado*1.8)+32;
			return gradoTransformado;
		}else {
			if(tipo=="Fahrenheit") {
				gradoTransformado=(grado-32)/1.8;
				return gradoTransformado;
			}
		}
		return 0;
	}
}