package cl.ubb.test1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PotenciaTest {

	@Before
	public void setUp() throws Exception {
	}
	
	@Test 
    public void testDeDosElevadoACeroRetornaCeroPruebaPrimerIF() { 
        String s = Potencia.povOf2(0); 
        assertEquals("0", s); 
    } 
	
	@Test 
    public void testDeDosElevadoAUnoDebeSerDosPruebaPrimerYTercerBucle() { 
        String s = Potencia.povOf2(1); 
        assertEquals("2", s); 
    } 
 
    @Test 
    public void testDeDosElevadoACuatroDebeSerDiesiseisPruebaPrimerYSegundoIF() { 
        String s = Potencia.povOf2(4); 
        assertEquals("16", s); 
    } 
    @Test 
    public void testDeDosElevadoACincoDebeSerTreintayDos() { 
        String s = Potencia.povOf2(5); 
        assertEquals("32", s); 
    } 
 
    @Test 
    public void testDeDosElevadoANueveEsQuinientosDoce() { 
        String s = Potencia.povOf2(9); 
        assertEquals("512", s); 
    }  

    @Test 
    public void testDeDosElevadoATreintaEs1073741824PruebaUnValorGrande() { 
        String s = Potencia.povOf2(30); 
        assertEquals("1073741824", s); 
    }  
}
