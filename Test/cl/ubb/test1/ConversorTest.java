package cl.ubb.test1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConversorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCincoGradosCelciusSonCuarentaYUnGradosF() {
		double esperado=41;
		double gradoATransformar=Conversor.Conversor("Celcius", 5);
		assertEquals(esperado, gradoATransformar,0);
	}
	
	@Test
	public void testCuarentaYUnGradosFahrenheitSonCincoGradosC() {
		double esperado=5;
		double gradoATransformar=Conversor.Conversor("Fahrenheit", 41);
		assertEquals(esperado, gradoATransformar,0);
	}
	
	@Test
	public void testTresFahrenheitSonMenosDiesiseisComaUnoUnoUnoUnoEtcEnNegativo() {
		double esperado=-16.11111111111111;
		double gradoATransformar=Conversor.Conversor("Fahrenheit", 3);
		assertEquals(esperado, gradoATransformar,0);
	}
	
	@Test
	public void TestCeroCelciusAFahrenheitSon32grados() {
		double esperado=32;
		double gradoATransformar=Conversor.Conversor("Celcius", 0);
		assertEquals(esperado, gradoATransformar,0);
	}
	
}
